/*

    stack
    Copyright (C) 2018  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
//implementation of stack functions 
//currently for int stacks
#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#define STACKSIZE 101

//initializes pointer to int array and first member to 1(points to top of the stack)
int *createStack(){
  int *i = malloc(sizeof(int) * STACKSIZE);
  *i = 1;
  return i;
}

// frees the stack
int freeStack(int *addr){
 free(addr);
 return 0;
}

//push to top of the stack
int push(int *addr, int value){
  if(*addr < STACKSIZE){
    printf("%d pointer value %d\n", *addr, value);
    *(addr + *addr) = value;
    *addr += 1;
    return 0;
  }
  return 1;
}

//peek at the top of the stack
int peek(int *addr){
  if(*addr > 0){
    return *(addr + *addr);
   }
}

//pop fromt the top of the stack
int pop(int *addr){
  if(*addr > 0){
    *addr -= 1;
    return *(addr + *addr + 1);
   }
}
