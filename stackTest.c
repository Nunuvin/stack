/*

    stack
    Copyright (C) 2018  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
//tests stack implementation

#include <stdio.h>
#include "stack.h"

/*
 *checks stack implementation
 */
int main(){

  //stack
  int *stack = createStack();
  push(stack, 1); 
  push(stack, 2);
  push(stack, 3);
  push(stack, 4);
  push(stack, 5);
  
  for(int i = 5; i>0;i--){
    int p = peek(stack);
    int t = pop(stack);

    if(p != t)
    printf("peeked %d popped %d supposed %d\n", p, t, i);
 }
  freeStack(stack);
  return 0;
}
